# -*- coding: utf-8 -*-

# provides tex2unicode and unicode2tex


################################################################
# LaTeX accents replacement
latexAccents = [
    ("à", "\\`a"),  # Grave accents
    ("è", "\\`e"),
    ("ì", "\\`\\i"),
    ("ò", "\\`o"),
    ("ù", "\\`u"),
    ("ỳ", "\\`y"),
    ("À", "\\`A"),
    ("È", "\\`E"),
    ("Ì", "\\`\\I"),
    ("Ò", "\\`O"),
    ("Ù", "\\`U"),
    ("Ỳ", "\\`Y"),
    ("á", "\\'a"),  # Acute accents
    ("é", "\\'e"),
    ("í", "\\'\\i"),
    ("ó", "\\'o"),
    ("ú", "\\'u"),
    ("ý", "\\'y"),
    ("Á", "\\'A"),
    ("É", "\\'E"),
    ("Í", "\\'\\I"),
    ("Ó", "\\'O"),
    ("Ú", "\\'U"),
    ("Ý", "\\'Y"),
    ("â", "\\^a"),  # Circumflex
    ("ê", "\\^e"),
    ("î", "\\^\\i"),
    ("ô", "\\^o"),
    ("û", "\\^u"),
    ("ŷ", "\\^y"),
    ("Â", "\\^A"),
    ("Ê", "\\^E"),
    ("Î", "\\^\\I"),
    ("Ô", "\\^O"),
    ("Û", "\\^U"),
    ("Ŷ", "\\^Y"),
    ("ä", '\\"a'),  # Umlaut or dieresis
    ("ë", '\\"e'),
    ("ï", '\\"\\i'),
    ("ö", '\\"o'),
    ("ü", '\\"u'),
    ("ÿ", '\\"y'),
    ("Ä", '\\"A'),
    ("Ë", '\\"E'),
    ("Ï", '\\"\\I'),
    ("Ö", '\\"O'),
    ("Ü", '\\"U'),
    ("Ÿ", '\\"Y'),
    ("ç", "\\c{c}"),  # Cedilla
    ("Ç", "\\c{C}"),
    ("œ", "{\\oe}"),  # Ligatures
    ("Œ", "{\\OE}"),
    ("æ", "{\\ae}"),
    ("Æ", "{\\AE}"),
    ("å", "{\\aa}"),
    ("Å", "{\\AA}"),
    ("–", "--"),  # Dashes
    ("—", "---"),
    ("ø", "{\\o}"),  # Misc latin-1 letters
    ("Ø", "{\\O}"),
    ("ß", "{\\ss}"),
    ("¡", "{!`}"),
    ("¿", "{?`}"),
    ("\\", "\\\\"),  # Characters that should be quoted
    ("~", "\\~"),
    ("&", "\\&"),
    ("$", "\\$"),
    ("{", "\\{"),
    ("}", "\\}"),
    ("%", "\\%"),
    ("#", "\\#"),
    ("_", "\\_"),
    ("©", "\copyright"),  # Misc
    ("ı", "{\\i}"),
    ("‘", "`"),  # Quotes
    ("’", "'"),
    ("“", "``"),
    ("”", "''"),
    ("‚", ","),
    ("„", ",,"),
    ("", ",,"),
]

mathModeLaTeX = [
    ("≥", "\\ge"),  # Math operators
    ("≤", "\\le"),
    ("≠", "\\neq"),
    ("µ", "\\mu"),
    ("°", "\\deg"),
    ("α", "\\alpha"),
    ("β", "\\beta"),
    ("γ", "\\gamma"),
    ("δ", "\\delta"),
    ("ϵ", "\\epsilon"),
    ("ζ", "\\zeta"),
    ("η", "\\eta"),
    ("θ", "\\theta"),
    ("ι", "\\iota"),
    ("κ", "\\kappa"),
    ("λ", "\\lambda"),
    ("μ", "\\mu"),
    ("ν", "\\nu"),
    ("ξ", "\\xi"),
    ("π", "\\pi"),
    ("ρ", "\\rho"),
    ("σ", "\\sigma"),
    ("τ", "\\tau"),
    ("υ", "\\upsilon"),
    ("ϕ", "\\phi"),
    ("χ", "\\chi"),
    ("ψ", "\\psi"),
    ("ω", "\\omega"),
    ("A", "\\Alpha"),
    ("B", "\\Beta"),
    ("Γ", "\\Gamma"),
    ("Δ", "\\Delta"),
    ("E", "\\Epsilon"),
    ("Ζ", "\\Zeta"),
    ("Η", "\\Eta"),
    ("Θ", "\\Theta"),
    ("Ι", "\\Iota"),
    ("Κ", "\\Kappa"),
    ("Λ", "\\Lambda"),
    ("Μ", "\\Mu"),
    ("Ν", "\\Nu"),
    ("Ξ", "\\Xi"),
    ("Π", "\\Pi"),
    ("Ρ", "\\Rho"),
    ("Σ", "\\Sigma"),
    ("Τ", "\\Tau"),
    ("ϒ", "\\Upsilon"),
    ("Φ", "\\Phi"),
    ("X", "\\Chi"),
    ("Ψ", "\\Psi"),
    ("Ω", "\\Omega"),
]


def addDollar(list):
    return ["$%s$" % x for x in list]


latexAccentsDict = dict(latexAccents)
latexAccentsDictR = dict(zip(latexAccentsDict.values(), latexAccentsDict.keys()))

tmp = dict(mathModeLaTeX)
mathModeLaTeXDictR = dict(zip(tmp.values(), tmp.keys()))
mathModeLaTeXDictR["$"] = ""  # just kill all inline math

mathModeLaTeXDict = dict(mathModeLaTeX)
mathModeLaTeXDict = dict(zip(mathModeLaTeXDict.keys(), addDollar(mathModeLaTeXDict.values())))


def string_replace(dct, text):
    keys = dct.keys()
    for n in keys:
        # if '%' not in text: break
        text = text.replace(n, dct[n])
    return text


def tex2unicode(s):
    s = string_replace(latexAccentsDictR, s)
    s = string_replace(mathModeLaTeXDictR, s)
    return s


def unicode2tex(s):
    s = string_replace(latexAccentsDict, s)
    s = string_replace(mathModeLaTeXDict, s)
    return s
