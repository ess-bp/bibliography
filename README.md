# Generator of publications list

We have our bibliography in Zotero, from which we generate an HTML containing our publications lists


 - [Our publications list](http://ess-bp.pages.esss.lu.se/bibliography)
 - [Commonly used references](http://ess-bp.pages.esss.lu.se/bibliography/ourrefs.html)
 - [References related to the ESSnuSB project](http://ess-bp.pages.esss.lu.se/bibliography/essnusb.html)


[Our Zotero group](https://www.zotero.org/groups/1048946)
