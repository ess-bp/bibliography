# settings.py

verbosity = 1

titlestring = "Bibliography of the beam physics section at ESS"
#### You must configure the following items

group_collection(1048946, collection="ZWVSAZGP")

sort_criteria = ["-date"]  # First by collection, then type, then by date, latest first.
# By type (journal, conference, etc.), then chronologically
# sort_criteria = ["type","-date"]   # we have date and type: First by date ("issued"), then by type.
# By year, then with journal articles first,"-date"
# sort_criteria = ["-year"]   # we have date and type: First by date ("issued"), then by type.
# By date only, newest first
# sort_criteria = ["-date"]   # we have date and type: First by date ("issued"), then by type.


###### Special settings


bib_style = "apa"  # bibliography style format (e.g., "apa" or "mla") - Any valid CSL style in the Zotero style repository

show_top_section_headings = 1  # show section headings for the first N sort criteria


write_full_html_header = True  # False to not output HTML headers.  In this case, expect a file in UTF-8 encoding.

outputfile = "public/index.html"  # relative or absolute path name of output file

show_search_box = True  # show a Javascript/JQuery based search box to filter pubs by keyword.  Must define jquery_path.
jquery_path = "site/jquery.min.js"  # path to jquery file on the server - default: wordpress location

number_bib_items = False  # show bibliographic items as numbered, ordered list

show_copy_button = True
clipboard_js_path = "site/clipboard.min.js"
copy_button_path = "site/clippy.svg"  # path to file on server

show_links = ["abstract", "pdf", "doi", "bib"]  # unconditionally show these items if they are available.

# show_shortcuts = [shortcut("collection", sortBy="name")]
show_shortcuts = [shortcut("year", [2023, 2022, 2021, "2019-2020", "2017-2018", "2014-2016", "-2013"])]
show_shortcuts += [shortcut("venue_short", sortDir="asc", topN=20)]
show_shortcuts += ["type"]

stylesheet_url = "style3.css"
